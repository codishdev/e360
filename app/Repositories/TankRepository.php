<?php 
namespace App\Repositories;

use App\Tank;

class TankRepository {
    
    protected $tank;

    public function __construct(Tank $tank) {
        $this->tank = $tank;
    }

    public function create($request) {
        return $this->tank->create($request);
    }

    public function tanks() {
        return $this->tank->with("location")->paginate(10);
    }

    public function getTank($tank_id) {
        return $this->tank->find($tank_id);
    }

    public function update($tank_id, array $request) {
        $tank = $this->tank->find($tank_id);
        if($tank->update($request)) {
            return $tank;
        }
    }

    public function deleteTank($tank_id) {
        $tank = $this->tank->find($tank_id);
        if($tank->delete()){
            return $tank;
        }
    }

}