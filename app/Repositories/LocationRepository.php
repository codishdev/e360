<?php 
namespace App\Repositories;

use App\Location;
use App\Tank;

class LocationRepository {
    protected $location;

    public function __construct(Location $location, Tank $tank) {
        $this->location = $location;
        $this->tank = $tank;
    }

    public function create($request) {
        $loc = $this->location->create($request);
        $this->tank->create($this->addTank($loc->id));
        return $loc;
    }

    public function locations() {
        return $this->location->with("tanks")->paginate(10);
    }

    public function getLocation($location_id) {
        return $this->location->find($location_id);
    }

    public function update($location_id, $request) {
        $location = $this->location->find($location_id);
        if($location->update($request)){
            return $location;
        }
    }

    public function deleteLocation($location_id) {
        $location = $this->location->find($location_id);
        if($location->delete()) {
            return $location;
        } 
    }
    
    public function addTank($id) {
       return  [
            "location_id" => $id,
            "tank_name" => "Tank 1",
            "tank_type" => "underground",
            "tank_volume" => "0",
            "status" => 1,
        ];
    }

}