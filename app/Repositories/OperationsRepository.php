<?php 
namespace App\Repositories;

use App\Location;
use App\Tank;
use Carbon\Carbon;

class OperationsRepository {
    
    protected $tank;
    protected $location;

    public function __construct(Tank $tank, Location $location) {
        $this->tank = $tank;
        $this->location = $location;
    }

    public function transfer_content($from, $to) {
        $sourceTank = $this->tank->find($from);
        $destinationTank = $this->tank->find($to);

        if($sourceTank->tank_volume == 0 ) {
            return response()->json("Cannot transfer from an empty tank");
        }

        if(strtolower($sourceTank->tank_type) === "underground" && strtolower($destinationTank->tank_type) === "underground") {
            $newDestinationTankVolume = $sourceTank->tank_volume + $destinationTank->tank_volume;
            if($destinationTank->update(["tank_volume" => $newDestinationTankVolume])) {
                $sourceTank->update(["tank_volume" => $sourceTank->tank_volume - $sourceTank->tank_volume]);
                return response()->json(["Source" => $sourceTank, "Destination" => $destinationTank]);
                //return response()->json("Content transfer ".$destinationTank->tank_name." was success full");
            }
            else {
                return response()->json("Error tranfering content");
            }
        } else { 
            return response()->json("Content can only be transfered between undergound tanks");
        }
    }

    public function getTanksVolume() {
        $locations =  $this->location->with("tanks")->get();
      
        $location = array();
        foreach ($locations as $key => $value) {
            $location["Total volume in ".$value->name." location as at ".$value->created_at] = 
            $value->with("tanks")->find($value->id)->tanks->sum("tank_volume")."Liters";
         
        }

        return $location;
    }

    public function loadContent($request) {

        $initialVolume = $this->tank->find($request["tank_id"])->tank_volume;
        $newVolume = $initialVolume + $request["volume"];

        $getTank = $this->tank->find($request["tank_id"]);
        $getTank->update(["tank_volume" => $newVolume]);

        return response()->json([
            "Volume before loading" => $initialVolume." liters",  
            "Volume after loading" => $getTank->tank_volume." liters",
            "Tank" => $getTank
            ]);
    }
    
}