<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tank extends Model
{
    protected $fillable = ["tank_name", "tank_type", "location_id", "tank_volume", "status"];

    public function location() {
        return $this->belongsTo(Location::class);
    }
}
