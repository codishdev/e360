<?php 
namespace App\Services;

use App\Repositories\OperationsRepository;
use App\Location;
use App\Tank;

class OperationServices {

    public function __construct(Tank $tank, Location $location, OperationsRepository $operation) {
        $this->tank = $tank;
        $this->location = $location;
        $this->operation = $operation;
    }

    public function transferContent($stank, $dtank) {
        return $this->operation->transfer_content($stank, $dtank);
    }

    public function locationVolume() {
        return $this->operation->getTanksVolume();
    }

    public function loadcontent($request) {
        return $this->operation->loadContent($request->all());
    }
}