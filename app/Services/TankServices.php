<?php
namespace App\Services;

use Illuminate\Http\Request;
use App\Repositories\TankRepository;
use App\Tank;

class TankServices {

    public function __construct(TankRepository $tank) {
        $this->tank = $tank;
    }

    public function index() {
        return $this->tank->tanks();
    }

    public function create(Request $request) {
        return $this->tank->create($request->all());
    }

    public function show($tank_id) {
        return $this->tank->getTank($tank_id);
    }

    public function update($tank_id, $request) {
        return $this->tank->update($tank_id, $request->all());
    }

    public function delete($tank_id) {
        return $this->tank->deleteTank($tank_id);
    }
}