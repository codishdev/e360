<?php
namespace App\Services;

use Illuminate\Http\Request;
use App\Repositories\LocationRepository;
use App\Location;

class LocationServices {

    public function __construct(locationRepository $location) {
        $this->location = $location;
    }

    public function index() {
        return $this->location->locations();
    }

    public function create(Request $request) {
        return $this->location->create($request->all());
    }

    public function show($location_id) {
        return $this->location->getLocation($location_id);
    }

    public function update($location_id, $request) {
        return $this->location->update($location_id, $request->all());
    }

    public function delete($location_id) {
        return $this->location->deleteLocation($location_id);
    }
}