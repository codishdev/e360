<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $fillable = ["name", "city", "status"];

    public function tanks() {
        return $this->hasMany(Tank::class);
    }
}
