<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TankRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->redirect = response()->json($this->messages());
        return [
            "location_id" => "required|integer",
            "tank_name" => "required",
            "tank_type" => "required",
            "tank_volume" => "required",
        ];
    }

    public function messages() {
        return [
            "location_id.required" => "Location id is required, make sure the feild is named location_id",
            "tank_name.required" => "make sure to name feild tank_name",
            "tank_type.required" => "make sure to name feild tank_type",
            "tank_volume.required" => "make sure to name feild tank_volume",
        ];
    }
}
