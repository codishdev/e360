<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoadContentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "location_id" => "required|integer",
            "tank_id" => "required|integer",
            "volume" => "required",
        ];
    }

    public function messages() {
        return [
            "location_id.required" => "Location id is required, make sure the feild is named location_id",
            "tank_id.required" => "make sure to name feild tank_id",
            "volume.required" => "make sure to name feild volume",
        ];
    }
}
