<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\TankServices;
use App\Tank;
use Validator;

class TankController extends Controller
{
    public function __construct(TankServices $tankservice) {
        $this->tankservice = $tankservice;
    }

    public function index() {
        return response()->json($this->tankservice->index());
    }

    public function store(Request $request) {
        $validate = Validator::make($request->all(), [
            "location_id" => "required|integer",
            "tank_name" => "required",
            "tank_type" => "required",
            "tank_volume" => "required"
        ], [
            "location_id.required" => "Location id is required, make sure the feild is named location_id",
            "tank_name.required" => "make sure to name feild tank_name",
            "tank_type.required" => "make sure to name feild tank_type",
            "tank_volume.required" => "make sure to name feild tank_volume",
        ]);

        if($validate->fails()) {
            return response()->json($validate->errors());
        }
        $this->tankservice->create($request);
        return response()->json("Tank added");
    }

    public function show($id) {
        return $this->tankservice->show($id);
    }

    public function update($id, Request $request) {
        return $this->tankservice->update($id, $request);
    }

    public function delete($id) {
        return $this->tankservice->delete($id);
    }
}
