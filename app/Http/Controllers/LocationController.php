<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\LocationServices;
use App\Location;
use Validator;

class LocationController extends Controller
{
    public function __construct(locationservices $locationservice) {
        $this->locationservice = $locationservice;
    }

    public function index() {
        return response()->json($this->locationservice->index());
    }

    public function store(Request $request) {
        $validate = Validator::make($request->all(), [
            "name" => "required",
        ], [
            "name.required" => "Location name is required, make sure the feild is named name",
        ]);
        
        if($validate->fails()) {
            return response()->json($validate->errors());
        }
        
        return response()->json($this->locationservice->create($request));
    }

    public function show($id) {
        return $this->locationservice->show($id);
    }

    public function update($id, Request $request) {
        return $this->locationservice->update($id, $request);
    }

    public function delete($id) {
        return $this->locationservice->delete($id);
    }
}
