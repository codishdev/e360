<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\OperationServices;
use Validator;

class OperationsController extends Controller
{
    public function __construct(OperationServices $operationservice) {
        $this->operationservice = $operationservice;
    }

    public function doTransfer(Request $request) {
        $validate = Validator::make($request->all(), [
            "stank" => "required|integer",
            "dtank" => "required|integer",
        ], [
            "stank.required" => "Source tank id is required, make sure the feild is named stank",
            "dtank.required" => "Destination tank id is required, make sure the feild is named dtank",
        ]);
        
        if($validate->fails()) {
            return response()->json($validate->errors());
        }

        return $this->operationservice->transferContent($request->stank, $request->dtank);
    }
    
    public function locationVolume() {
        return $this->operationservice->locationVolume();
    }

    public function load_content(Request $request) {
        $validate = Validator::make($request->all(), [
            "location_id" => "required|integer",
            "tank_id" => "required|integer",
            "volume" => "required",
        ], [
            "location_id.required" => "Location id is required, make sure the feild is named location_id",
            "tank_id.required" => "Tank id is required, make sure the feild is named tank_id",
            "volume.required" => "Volume is required, make sure the feild is named volume",
        ]);
        
        if($validate->fails()) {
            return response()->json($validate->errors());
        }

        return $this->operationservice->loadcontent($request);
    }
}
