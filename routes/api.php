<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(["prefix" => "v1.0"], function() {

    /**
     * Tank CRUD routes
     */
    Route::get("/tanks", "TankController@index");
    Route::post("/tank/create", "TankController@store");
    Route::get("/tank/{id}", "TankController@show");
    Route::put("/tank/{id}", "TankController@update");
    Route::delete("/tank/{id}", "TankController@delete");

    /**
     * Location CRUD routes
     */
    Route::get("/locations", "LocationController@index");
    Route::post("/location/create", "LocationController@store")->name("location.store");
    Route::get("/location/{id}", "LocationController@show");
    Route::put("/location/{id}", "LocationController@update");
    Route::delete("/location/{id}", "LocationController@delete");


    /**
     * Operation routes
     */
    Route::post("/transfer-content", "OperationsController@doTransfer");
    Route::post("/load-content", "OperationsController@load_content");
    Route::get("/daily-volume", "OperationsController@dailyVolume");
    Route::get("/", "OperationsController@locationVolume");
});
