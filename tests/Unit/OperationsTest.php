<?php 
namespace Tests\Unit;

use Tests\TestCase;
use App\Repositories\OperationsRepository;
use App\Repositories\LocationRepository;
use App\Repositories\TankRepository;
use App\Location;
use App\Tank;

class OperationsTest extends TestCase
{
    protected $operationRepo;
    protected $location;
    protected $tank;
    protected $locationRepo;
    protected $tankRepo;
    protected $sourceTank;
    protected $destTank;

    /** @test */
    public function can_transfer_content() {
        $data = [
            "location_id" => $this->faker->randomDigit,
            "tank_name" => "Tank 1",
            "tank_type" => "underground",
            "tank_volume" => 500,
            "status" => 1
        ];
        $data2 = [
            "location_id" => 3,
            "tank_name" => "Tank 1", 
            "tank_type" => "underground",
            "tank_volume" => 7000,
            "status" => 1
        ];


        $this->operationRepo = new OperationsRepository( new Tank, new Location);
        $this->tankRepo = new TankRepository(new Tank);
        $this->sourceTank = $this->tankRepo->create($data);
        $this->destTank = $this->tankRepo->create($data2);
        
        $sourceTank = $this->tankRepo->getTank($this->sourceTank->id);
        $destTank = $this->tankRepo->getTank($this->destTank->id);

        $transfer = $this->operationRepo->transfer_content($sourceTank->id, $destTank->id);

        $this->assertInstanceOf(Tank::class, $this->sourceTank);
        $this->assertInstanceOf(Tank::class, $this->destTank);
        $this->assertEquals($sourceTank->tank_type, $destTank->tank_type);
  
    }

    /** @test */
    public function can_load_content() {       

        $data = [
            "location_id" => $this->faker->randomDigit,
            "tank_name" => "Tank 1",
            "tank_type" => "underground",
            "tank_volume" => 500,
            "status" => 1
        ];

        $this->operationRepo = new OperationsRepository( new Tank, new Location);
        $this->tankRepo = new TankRepository(new Tank);
        $this->tank = $this->tankRepo->create($data);
        $tank = $this->tankRepo->getTank($this->tank->id); 

        $data2 = [
            "tank_id" => $tank->id,
            "volume" => 7000
        ];
        $loadcontent = $this->operationRepo->loadContent($data2);
        $this->assertInstanceOf(Tank::class, $this->tank);
    }

    /** @test */
    public function can_get_daily_volume() {
        $this->operationRepo = new OperationsRepository( new Tank, new Location);
        $volumes = $this->operationRepo->getTanksVolume();

        $this->assertIsIterable($volumes);
    }

}