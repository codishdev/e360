<?php 
namespace Tests\Unit;

use Tests\TestCase;
use App\Repositories\LocationRepository;
use App\Location;
use App\Tank;

class LocationTest extends TestCase
{
    protected $locationRepo;
    protected $location;
    protected $tank;

    /** @test */
    public function can_add_location() {
        $data = [
            "name" => "Yaba",
            "city" => "Lagos",
            "status" => 1
        ];

        $this->locationRepo = new LocationRepository(new Location, new Tank);
        $this->location = $this->locationRepo->create($data);
        $tank = new Tank;
        $location = new Location;

        $tankData = [
            "location_id" => $this->location->id,
            "tank_name" => "Tank 1",
            "tank_type" => "underground",
            "tank_volume" => 0,
            "status" => 1
        ];
        
        $this->tank = $tank->create($tankData);

        $this->assertInstanceOf(Location::class, $this->location);
        $this->assertEquals($data["name"], $this->location->name);
        $this->assertEquals($data["city"], $this->location->city);
        $this->assertEquals($data["status"], $this->location->status);
        $this->assertInstanceOf(Tank::class, $this->tank);
    }

    /** @test */
    public function can_show_location() {
        $location = factory(Location::class)->create();
        $this->locationRepo = new LocationRepository(new Location, new Tank);
        $this->location = $this->locationRepo->getLocation($location->id);
        
        $this->assertInstanceOf(Location::class, $this->location);
        $this->assertEquals($this->location->name, $location->name);
        $this->assertEquals($this->location->city, $location->city);
        $this->assertEquals($this->location->status, $location->status);

    }

    /** @test */
    public function can_update_location() {

        $data = [
            "name" => $this->faker->name,
            "city" => $this->faker->text(9),
            "status" => 1
        ];

        $location = factory(Location::class)->create();
        $this->locationRepo = new LocationRepository(new Location, new Tank);
        $update = $this->locationRepo->update($location->id, $data);
        
        $this->assertTrue($update);

    }

    /** @test */
    public function can_delete_location() {

        $location = factory(Location::class)->create();
        $this->locationRepo = new LocationRepository(new Location, new Tank);
        $delete = $this->locationRepo->deleteLocation($location->id);
        
        $this->assertTrue($delete);
    }


}