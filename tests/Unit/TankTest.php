<?php 
namespace Tests\Unit;

use Tests\TestCase;
use App\Repositories\TankRepository;
use App\Tank;

class TankTest extends TestCase
{
    protected $tankRepo;
    protected $tank;

    /** @test */
    public function can_add_tank() {
        
        $data = [
            "location_id" => $this->faker->randomDigit,
            "tank_name" => "Tank 1",
            "tank_type" => "underground",
            "tank_volume" => 0,
            "status" => 1
        ];
        $this->tankRepo = new TankRepository(new Tank);
        $this->tank = $this->tankRepo->create($data);
        

        $this->assertInstanceOf(Tank::class, $this->tank);
        $this->assertEquals($data["location_id"], $this->tank->location_id);
        $this->assertEquals($data["tank_name"], $this->tank->tank_name);
        $this->assertEquals($data["tank_type"], $this->tank->tank_type);
        $this->assertEquals($data["tank_volume"], $this->tank->tank_volume);
        $this->assertEquals($data["status"], $this->tank->status);
    }

    /** @test */
    public function can_show_tank() {
        $tank = factory(Tank::class)->create();
        $this->tankRepo = new TankRepository(new Tank);
        $this->tank = $this->tankRepo->getTank($tank->id);
        
        $this->assertInstanceOf(Tank::class, $this->tank);
        $this->assertEquals($this->tank->location_id, $tank->location_id);
        $this->assertEquals($this->tank->tank_name, $tank->tank_name);
        $this->assertEquals($this->tank->tank_type, $tank->tank_type);
        $this->assertEquals($this->tank->tank_volume, $tank->tank_volume);
        $this->assertEquals($this->tank->status, $tank->status);

    }

    /** @test */
    public function can_update_tank() {

        $data = [
            "location_id" => 1,
            "tank_name" => "tank 2",
            "tank_type" => "yaba",
            "tank_volume" => 2000,
            "status" => 1
        ];

        $tank = factory(Tank::class)->create();
        $this->tankRepo = new TankRepository(new Tank);
        $update = $this->tankRepo->update($tank->id, $data);
        
        $this->assertTrue($update);

    }

    /** @test */
    public function can_delete_tank() {

        $tank = factory(Tank::class)->create();
        $this->tankRepo = new TankRepository(new Tank);
        $delete = $this->tankRepo->deleteTank($tank->id);
        
        $this->assertTrue($delete);
    }


}