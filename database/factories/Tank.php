<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Tank;
use Faker\Generator as Faker;

$factory->define(Tank::class, function (Faker $faker) {

     return [
        "location_id" => $faker->randomDigit,
        "tank_name" => $faker->name,
        "tank_type" => "underground",
        "tank_volume" => 2000,
        "status" => 1
    ];
});
